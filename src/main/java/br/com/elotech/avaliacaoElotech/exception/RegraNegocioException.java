package br.com.elotech.avaliacaoElotech.exception;

public class RegraNegocioException extends RuntimeException{

    public RegraNegocioException(String msg){
        super(msg);
    }

}
