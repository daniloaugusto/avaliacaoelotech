package br.com.elotech.avaliacaoElotech.service.impl;

import br.com.elotech.avaliacaoElotech.exception.RegraNegocioException;
import br.com.elotech.avaliacaoElotech.model.entity.Pessoa;
import br.com.elotech.avaliacaoElotech.model.repository.PessoaRepository;
import br.com.elotech.avaliacaoElotech.service.PessoaService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintValidator;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class PessoaServiceImpl implements PessoaService {

    private PessoaRepository repository;
    private  LocalDate dataAtual;

    public PessoaServiceImpl(PessoaRepository repository){
        super();
        this.repository = repository;
    }

    @Override
    public void validar(Pessoa pessoa) {

        if (pessoa.getDatanascimento() ==null || pessoa.getDatanascimento().toString().equals("")){
            throw new RegraNegocioException("Informe uma data de Nascimento Valida");
        }
        if (pessoa.getDatanascimento().equals(dataAtual) || pessoa.getDatanascimento().compareTo(dataAtual) ==1 ) {
            throw new RegraNegocioException("Data de nascimento deve ser menor ou diferente da data Atual");
        }
        if (pessoa.getCpf().equals("")|| pessoa.getCpf() ==null || pessoa.getCpf().length() !=11){
            throw new RegraNegocioException("Informe um cpf Valido.");
        }
        if (pessoa.getCpf().equals("00000000000") || pessoa.getCpf().equals("11111111111")
            || pessoa.getCpf().equals("22222222222") || pessoa.getCpf().equals("33333333333")
            || pessoa.getCpf().equals("44444444444") || pessoa.getCpf().equals("55555555555")
            || pessoa.getCpf().equals("66666666666") || pessoa.getCpf().equals("77777777777")
            || pessoa.getCpf().equals("88888888888") || pessoa.getCpf().equals("99999999999")){
            throw new RegraNegocioException("Informe um cpf Valido.");
        }
    }

    @Override
    @Transactional
    public Pessoa salvar(Pessoa pessoa) {
        return repository.save(pessoa);
    }

    @Override
    @Transactional
    public Pessoa atualizar(Pessoa pessoa) {
        Objects.requireNonNull(pessoa.getId());
        return repository.save(pessoa);
    }

    @Override
    @Transactional
    public List<Pessoa> consultar(Pessoa PessoaFiltro) {
        Example example = Example.of(PessoaFiltro, ExampleMatcher
                        .matching()
                        .withIgnoreCase()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING));

        return repository.findAll(example);
    }

    @Override
    @Transactional
    public void deletar(Pessoa pessoa) {
        Objects.requireNonNull(pessoa.getId());
        repository.delete(pessoa);

    }

}