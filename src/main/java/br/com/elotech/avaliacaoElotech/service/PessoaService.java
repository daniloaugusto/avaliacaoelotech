package br.com.elotech.avaliacaoElotech.service;

import br.com.elotech.avaliacaoElotech.model.entity.Pessoa;

import java.time.Instant;
import java.util.List;

public interface PessoaService {

    Pessoa salvar(Pessoa pessoa);

    Pessoa atualizar(Pessoa pessoa);

    List<Pessoa> consultar(Pessoa PessoaFiltro);

    void validar(Pessoa pessoa);

    void deletar(Pessoa pessoa);

}
