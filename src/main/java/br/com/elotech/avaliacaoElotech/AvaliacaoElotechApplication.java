package br.com.elotech.avaliacaoElotech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvaliacaoElotechApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvaliacaoElotechApplication.class, args);
	}

}
