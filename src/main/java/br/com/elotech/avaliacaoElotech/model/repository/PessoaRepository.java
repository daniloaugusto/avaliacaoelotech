package br.com.elotech.avaliacaoElotech.model.repository;

import br.com.elotech.avaliacaoElotech.model.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PessoaRepository  extends JpaRepository<Pessoa, Long> {

    boolean existsBycpf(String cpf);

    Optional<Pessoa>findBycpf(String cpf);

}
