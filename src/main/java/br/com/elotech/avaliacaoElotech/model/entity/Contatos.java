package br.com.elotech.avaliacaoElotech.model.entity;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;

@Entity
@Table(name = "contatos", schema = "aise")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contatos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull
    private Long id;

    @Column(name ="nome")
    @NotNull
    private String nome;

    @Column(name = "telefone")
    @NotNull
    private Integer telefone;

    @Column(name = "email")
    @NotNull
    private String email;

    @ManyToOne
    @JoinColumn(name ="id_pessoa")
    private Pessoa id_pessoa;

}
