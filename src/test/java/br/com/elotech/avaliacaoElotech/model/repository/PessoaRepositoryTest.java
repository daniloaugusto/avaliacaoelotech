package br.com.elotech.avaliacaoElotech.model.repository;

import br.com.elotech.avaliacaoElotech.model.entity.Pessoa;
import lombok.Data;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Data
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class PessoaRepositoryTest {

    public static  Pessoa criarPessoa(){

        return Pessoa.builder()
                .nome("Sebastiao")
                .cpf("45096505073")
                .datanascimento(LocalDate.of(1994,12,9))
                .build();
    }

    @Autowired
    PessoaRepository repository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    //@DisplayName("verificar a Existencia do Nome ")
    public void nomeExiste(){
        //cenario
        Pessoa pessoa = criarPessoa();
        entityManager.persist(pessoa);
        //acao
        boolean result = repository.existsBycpf("45096505073");

        //verificacao
        Assertions.assertThat(result).isTrue();

    }

}
